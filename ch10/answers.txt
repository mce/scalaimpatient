1: [The java.awt.Rectangle class has useful methods translate and grow that are un-
fortunately absent from classes such as java.awt.geom.Ellipse2D. In Scala, you
can ﬁx this problem. Deﬁne a trait RectangleLike with concrete methods translate
and grow. Provide any abstract methods that you need for the implementation,
so that you can mix in the trait like this:
val egg = new java.awt.geom.Ellipse2D.Double(5, 10, 20, 30) with RectangleLike
egg.translate(10, -10)
egg.grow(10, 20)]

object Main extends App {
  trait RectangleLike extends java.awt.geom.Ellipse2D.Double {
    
    def translate(x: Int, y: Int) {
      this.x += x
      this.y += y
    }
    
    def grow(width: Int, height: Int) {
      this.width += width
      this.height += height
    }
  }
  
  val egg = new 
    java.awt.geom.Ellipse2D.Double(5, 10, 20, 30) with RectangleLike
      egg.translate(10, -10)
      egg.grow(10, 20)
      
  println(egg.getX())
  println(egg.getY())
  println(egg.getWidth())
  println(egg.getHeight())
}

2: [Deﬁne a class OrderedPoint by mixing scala.math.Ordered[Point] into java.awt.Point.
Use lexicographic ordering, i.e. (x, y) < (x’, y’) if x < x’ or x = x’ and y < y’.]

import java.awt.Point
import scala.math.Ordered

object Main extends App {
  class OrderedPoint(x: Int, y: Int) extends Point(x, y) with Ordered[Point] {
    def compare(that: Point) = {
       if (x < that.x || (x == that.x && y < that.y))
         -1
       else if (x > that.x || (x == that.x && y > that.y))
         1
       else
         0 
    }
  }
  
  val op1 = new OrderedPoint(5, 10)
  val op2 = new OrderedPoint(10, 5)
  
  print (op1 < op2)
}

3: [Look at the BitSet class, and make a diagram of all its superclasses and traits.
Ignore the type parameters (everything inside the [...]). Then give the
linearization of the traits.]

In the REPL:
scala -Dscala.repl.power
intp.types("scala.collection.BitSet").tpe.baseTypeSeq.toList >

This gives:

scala.collection.BitSet
scala.collection.Set[Int]
Iterable[Int]
scala.collection.BitSetLike[scala.collection.BitSet]
scala.collection.SetLike[Int,scala.collection.BitSet] with scala.collection.SetLike[Int,scala.collection.Set[Int]]
scala.collection.GenSet[Int]
Traversable[Int]
scala.collection.IterableLike[Int,scala.collection.BitSet] with scala.collection.IterableLike[Int,Iterable[Int]] with scala.collection.IterableLike[Int,scala.collection.Set[Int]]
scala.collection.GenIterable[Int]
scala.collection.GenSetLike[Int,scala.collection.BitSet] with scala.collection.GenSetLike[Int,scala.collection.GenSet[Int]] with scala.collection.GenSetLike[Int,scala.collection.Set[Int]]
scala.collection.TraversableLike[Int,scala.collection.BitSet] with scala.collection.TraversableLike[Int,Iterable[Int]] with scala.collection.TraversableLike[Int,Traversable[Int]] with scala.collection.TraversableLike[Int,scala.collection.Set[Int]]
scala.collection.GenTraversable[Int]
scala.collection.GenIterableLike[Int,scala.collection.BitSet] with scala.collection.GenIterableLike[Int,Iterable[Int]] with scala.collection.GenIterableLike[Int,scala.collection.GenIterable[Int]] with scala.collection.GenIterableLike[Int,scala.collection.GenSet[Int]] with scala.collection.GenIterableLike[Int,scala.collection.Set[Int]]
scala.collection.GenTraversableLike[Int,scala.collection.BitSet] with scala.collection.GenTraversableLike[Int,Iterable[Int]] with scala.collection.GenTraversableLike[Int,scala.collection.GenIterable[Int]] with scala.collection.GenTraversableLike[Int,Traversable[Int]] with scala.collection.GenTraversableLike[Int,scala.collection.GenTraversable[Int]] with scala.collection.GenTraversableLike[Int,scala.collection.GenSet[Int]] with scala.collection.GenTraversableLike[Int,scala.collection.Set[Int]]
scala.collection.generic.GenericSetTemplate[Int,scala.collection.GenSet] with scala.collection.generic.GenericSetTemplate[Int,scala.collection.Set]
scala.collection.TraversableOnce[Int]
scala.collection.generic.GenericTraversableTemplate[Int,Iterable] with scala.collection.generic.GenericTraversableTemplate[Int,scala.collection.GenIterable] with scala.collection.generic.GenericTraversableTemplate[Int,scala.collection.GenTraversable] with scala.collection.generic.GenericTraversableTemplate[Int,Traversable] with scala.collection.generic.GenericTraversableTemplate[Int,scala.collection.GenSet] with scala.collection.generic.GenericTraversableTemplate[Int,scala.collection.Set]
Int => Boolean
scala.collection.GenTraversableOnce[Int]
scala.collection.Parallelizable[Int,scala.collection.parallel.ParIterable[Int]] with scala.collection.Parallelizable[Int,scala.collection.parallel.ParSet[Int]]
scala.collection.generic.Subtractable[Int,scala.collection.BitSet] with scala.collection.generic.Subtractable[Int,scala.collection.Set[Int]]
ScalaObject
Equals
scala.collection.generic.FilterMonadic[Int,scala.collection.BitSet] with scala.collection.generic.FilterMonadic[Int,Iterable[Int]] with scala.collection.generic.FilterMonadic[Int,Traversable[Int]] with scala.collection.generic.FilterMonadic[Int,scala.collection.Set[Int]]
scala.collection.generic.HasNewBuilder[Int,scala.collection.BitSet] with scala.collection.generic.HasNewBuilder[Int,Iterable[Int]] with scala.collection.generic.HasNewBuilder[Int,scala.collection.GenIterable[Int]] with scala.collection.generic.HasNewBuilder[Int,Traversable[Int]] with scala.collection.generic.HasNewBuilder[Int,scala.collection.GenTraversable[Int]] with scala.collection.generic.HasNewBuilder[Int,scala.collection.GenSet[Int]] with scala.collection.generic.HasNewBuilder[Int,scala.collection.Set[Int]]
java.lang.Object
Any

4: [Provide a CryptoLogger class that encrypts the log messages with the Caesar
cipher. The key should be 3 by default, but it should be overridable by the
user. Provide usage examples with the default key and a key of –3.]

object Main extends App {
  trait Logger {
    def log(msg: String)
  }

  class CryptoLogger extends {
    private val alphaU = 'A' to 'Z'
    private val alphaL = 'a' to 'z'
    
    def log(msg: String) {
      println(encode(msg, -3))
    }
    
    def encode(text:String, key:Int) = text.map{
      case c if alphaU.contains(c) => rot(alphaU, c, key)
      case c if alphaL.contains(c) => rot(alphaL, c, key)
      case c => c
    }
    
    private def rot(a:IndexedSeq[Char], c:Char, key:Int) = 
      a((c - a.head + key + a.size) % a.size)
  }
  
  val cipher = new CryptoLogger
  
  cipher.log("The five boxing wizards jump quickly")
}

5: [The JavaBeans speciﬁcation has the notion of a property change listener, a
standardized way for beans to communicate changes in their properties. The
PropertyChangeSupport class is provided as a convenience superclass for any bean
that wishes to support property change listeners. Unfortunately, a class that
already has another superclass—such as JComponent—must reimplement the
methods. Reimplement PropertyChangeSupport as a trait, and mix it into
the java.awt.Point class.]

import scala.collection.mutable.Map
import java.beans.PropertyChangeListener
import java.beans.PropertyChangeEvent

trait PropertyChangeSupport {
  val listeners = Map[String, PropertyChangeListener]()
   
  def addPropertyChangeListener(propertyName: String, 
    listener: PropertyChangeListener) {
    listeners += propertyName -> listener
  }
  
  def firePropertyChange(propertyName: String, 
      oldValue: Any, 
      newValue: Any) {
    if (listeners.contains(propertyName)) {
      val listener = listeners(propertyName)
      listener.propertyChange(new PropertyChangeEvent(
        this, propertyName, oldValue, newValue))
    }
  }
}

class Listener extends PropertyChangeListener {
  def propertyChange(evt: PropertyChangeEvent) {
    println(evt)
  }
}

object Main extends App {  
  val point = new java.awt.Point(10, 20) with PropertyChangeSupport {
    override def setLocation(x: Double, y: Double) {
      firePropertyChange("setLocation", (getX(), getY()),
        (x, y))
      super.setLocation(x, y)
    }
  }
  
  val listener = new Listener
  
  point.addPropertyChangeListener("setLocation", listener)
  
  point.setLocation(20.0, 30.0)
}

6: [In the Java AWT library, we have a class Container, a subclass of Component that
collects multiple components. For example, a Button is a Component, but a Panel
is a Container. That’s the composite pattern at work. Swing has JComponent and
JContainer, but if you look closely, you will notice something strange. JComponent
extends Container, even though it makes no sense to add other components to,
say, a JButton. The Swing designers would have ideally preferred the design
in Figure 10–4.
But that’s not possible in Java. Explain why not. How could the design be
executed in Scala with traits?]

The class design describe in figure 10-4 is impossible in Java, as Java would not allow JContainer to inherit from the classes JComponet and Container simultaneously - in other words, Java does not allow multiple inheritance of classes, only interfaces. You could achieve the proposed design by making Component and Container traits, meaning that JComponent could extend Component, whilst JContainer could extend Component and Container.

7: [There are dozens of Scala trait tutorials with silly examples of barking dogs
or philosophizing frogs. Reading through contrived hierarchies can be
tedious and not very helpful, but designing your own is very illuminating.
Make your own silly trait hierarchy example that demonstrates layered
traits, concrete and abstract methods, and concrete and abstract ﬁelds.]

import collection.mutable.ArrayBuffer
import collection.mutable.Map

trait FrogLike {
  def croak(duration: Int)
}

trait PoisonousLike {
  val kills : Map[String, Boolean] 
  
  def canKill(name: String) : Boolean
}

trait BrightlyColoredLike {
  val colors = new ArrayBuffer[java.awt.Color]()
  
  def addColor(color: java.awt.Color) {
    colors += color
  }
}

class PoisonDartFrog extends FrogLike 
  with PoisonousLike with BrightlyColoredLike {
  val kills = Map[String, Boolean]("human" -> true, 
    "horse" -> true, 
    "liophis_snake" -> false)
  
  def croak(duration: Int) {
    println("Ribbit!! " * duration)
  }
  
  def canKill(name: String) = {
    kills(name)
  }
}

object Main extends App {   
  val frog = new PoisonDartFrog
  frog.addColor(java.awt.Color.blue)
  frog.addColor(java.awt.Color.yellow)
  frog.addColor(java.awt.Color.red)
  println(frog.canKill("human"))
  println(frog.canKill("liophis_snake"))
  println(frog.croak(5))
}

8: [In the java.io library, you add buffering to an input stream with a BufferedInputStream decorator. Reimplement buffering as a trait. For simplicity, override the read method.]

import java.io.FileInputStream
import java.io.InputStream
import java.io.File

trait Bufferable {
  this: FileInputStream =>
  
  var buffer = new Array[Byte](16)
  var pos = 0
  var copied = 0
  
  override def read : Int = {  
    if ((pos == 0) || (available >= 16 && pos >= 16)) {
      copied = 16
      pos = 0
      read(buffer, 0, copied)
    }
    else if (available > 0 && available < 16) {
      copied = available
      pos = 0
      read(buffer, 0, copied)
    }
    
    var ret = -1
    if (pos < copied) {
      ret = buffer(pos)
      pos += 1
    }
    ret
  }
}

object Main extends App {   
  val bfis = new FileInputStream(new File("/home/mcdamon/Downloads/test.txt"))
    with Bufferable
  
  var c : Int = 0
  while ({ c = bfis.read; c != -1}) {
    print(c.toChar)
  }
  
  bfis.close
}

9: [Using the logger traits from this chapter, add logging to the solution of the preceding problem that demonstrates buffering.]

import java.io.FileInputStream
import java.io.InputStream
import java.io.File

trait Logger {
  def log(msg: String)
}

trait TimestampLogger extends Logger {
  abstract override def log(msg: String) {
    super.log(new java.util.Date() + " " + msg)
  }
}

trait ConsoleLogger extends Logger {
  override def log(msg: String) {
    println(msg)
  }
}

trait Bufferable extends Logger {
  this: FileInputStream =>
  
  var buffer = new Array[Byte](16)
  var pos = 0
  var copied = 0
  
  override def read : Int = {  
    if ((pos == 0) || (available >= 16 && pos >= 16)) {
      copied = 16
      pos = 0
      read(buffer, 0, copied)
      log("read " + buffer)
    }
    else if (available > 0 && available < 16) {
      copied = available
      pos = 0
      read(buffer, 0, copied)
      log("read " + buffer)
    }
    
    var ret = -1
    if (pos < copied) {
      ret = buffer(pos)
      pos += 1
    }
    ret
  }
}

object Main extends App {   
  val bfis = new FileInputStream(new File("/home/mcdamon/Downloads/test.txt"))
    with Bufferable with ConsoleLogger with TimestampLogger
  
  var c : Int = 0
  while ({ c = bfis.read; c != -1}) {
    print(c.toChar)
  }
  
  bfis.close
}

10: [Implement a class IterableInputStream that extends java.io.InputStream with the trait Iterable[Byte].]

import java.io.FileInputStream
import java.io.InputStream
import java.io.File
import java.io.FileDescriptor
import collection.Iterable

class IterableInputStream(val is: InputStream) 
  extends InputStream with Iterable[Int] {
  def iterator: Iterator[Int] = new Iterator[Int] {
    private var i = 1
    def hasNext: Boolean =  {
      is.available > 0
    }
    def next() : Int = {
      val n = is.read
      i += 1
      n
    }
  }
  
  def read = {
    is.read
  }
}

object Main extends App {   
  val iis = new IterableInputStream(new FileInputStream("/home/mcdamon/Downloads/test.txt"))
  
  for (b <- iis)
    println(b.toChar)
  
  iis.close
}
